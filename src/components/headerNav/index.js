import React, {Component} from 'react'
import ModuleStyle from '../headerNav/headerNav.module.css'
import SideNav from '../sideNav'
import Button from 'react-bootstrap/Button'
import ButtonToolbar from 'react-bootstrap/ButtonToolbar'


export default class HeaderNav extends Component{
    constructor(props){
        super(props)

        this.state = {navOn: false}

    }

    buttomClick(){

        this.setState({navOn: !this.state.navOn})


    }



    render(){

        if(true){
            return (

                <div className={ModuleStyle.HeaderNav} id='headerTarget'>

                    <div className={ModuleStyle.NavMenu} onClick={this.buttomClick.bind(this)}>
                      <img src='https://picsum.photos/100' alt='didnt load' style={{maxHeight:'100%', maxWidth:'100%'}}></img>
                    </div>

                    <container fluid="true" >
                      <ButtonToolbar style={{maxHeight:'100%'}}>
                        <Button type="button" style={{maxHeight:'100%', fontSize:'40%', minHeight:'15vh', minWidth:'30vh'}}>Demo</Button>
                        <Button type="submit" style={{maxHeight:'100%', fontSize:'40%', minHeight:'15vh', minWidth:'30vh'}}>header</Button>
                        <Button as="input" type="button" style={{maxHeight:'100%', fontSize:'40%', minHeight:'15vh', minWidth:'30vh'}}value="Nav" />

                      </ButtonToolbar>
                    </container>


                    <SideNav isOpen={this.state.navOn} onClick={this.buttomClick.bind(this)}/>


                </div>


            )
        }
    }




}
