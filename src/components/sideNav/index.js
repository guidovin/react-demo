import React, {Component} from 'react'
import ModuleStyle from '../sideNav/c.module.css'

import Button from 'react-bootstrap/Button'
import ButtonGroup from 'react-bootstrap/ButtonGroup'
import Dropdown from 'react-bootstrap/Dropdown'
import DropdownButton from 'react-bootstrap/DropdownButton'

export default class SideNav extends Component{


    render(){

        if(this.props.isOpen){
            return (

                <div className={ModuleStyle.sideNav} onClick={this.props.onClick}>


                  <ButtonGroup vertical style={{position:'absolute', display:'inline-block', width:'100%'}}>
                    <div style={{display:'inline-block', width:'100%'}} ><p>Side Nav menu </p></div>
                    <Button>Button</Button>
                    <Button>Button</Button>
                    <DropdownButton as={ButtonGroup} title="Dropdown" id="bg-vertical-dropdown-1">
                      <Dropdown.Item eventKey="1">Dropdown link</Dropdown.Item>
                      <Dropdown.Item eventKey="2">Dropdown link</Dropdown.Item>
                    </DropdownButton>
                    <Button>Button</Button>
                    <Button>Button</Button>
                    <DropdownButton as={ButtonGroup} title="Dropdown" id="bg-vertical-dropdown-2">
                      <Dropdown.Item eventKey="1">Dropdown link</Dropdown.Item>
                      <Dropdown.Item eventKey="2">Dropdown link</Dropdown.Item>
                    </DropdownButton>
                    <DropdownButton as={ButtonGroup} title="Dropdown" id="bg-vertical-dropdown-3">
                      <Dropdown.Item eventKey="1">Dropdown link</Dropdown.Item>
                      <Dropdown.Item eventKey="2">Dropdown link</Dropdown.Item>
                    </DropdownButton>
                  </ButtonGroup>

                </div>


            )
        }
        else return <div className={ModuleStyle.noSideNav}></div>
    }




}
